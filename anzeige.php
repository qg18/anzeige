<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="https://api.laut.fm/js_tools/lautfm_js_tools.0.9.1.js" ></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
        
        <!-- Aktueller Titel / Interpret -->
        <div class="col-md-12">
            <div id="song"></div>
            <script type="text/html" id="song_template" charset="utf-8">
            <p>Du h&ouml;rst<br /><strong><%= this.title %></strong><br />
            von <em><%= this.artist.name %> </p> </script> 
            <script type="text/javascript" charset="utf-8"> laut.fm.station('HIER DEINE STATION') 
            .info({container:'station', template:'station_template'}) 
            .current_song({container:'song', template:'song_template'}, true); </script>
            </div>
        </div>

        <!-- 10 zuletzt gespielten Lieder -->
        <div class="col-md-12">
            <ul id="songs"></ul> <script type="text/html" id="songs_template" charset="utf-8"> 
            <% this.forEach(function(song){ %>
            <li><%= song.started_at.humanTimeLong() %> Uhr: <strong><%= song.title %></strong><br />
            von <%= song.artist.name %>, <em><%= song.album %></em> (<%= song.releaseyear %>)<br />
            &raquo; <a href="http://www.amazon.de/gp/search?ie=UTF8&keywords=<%= encodeURIComponent(song.artist.name) %>%20<%= encodeURIComponent(song.title) %>&tag=celtic-rock-21&index=music&linkCode=ur2&camp=1638&creative=6742" target="_blank" title="Auf Amazon nach diesem Interpreten suchen">Amazon</a> 
            | <a href="http://www.google.de/?q=<%= encodeURIComponent(song.artist.name) %>%20<%= encodeURIComponent(song.title) %>" title="Titel auf google.de suchen" target="_blank">Google</a>
            | <a href="http://www.youtube.com/results?search_query=<%= encodeURIComponent(song.artist.name) %>%20<%= encodeURIComponent(song.title) %>" title="Band auf Youtube suchen" target="_blank">Youtube</a> 
            <br /><br /></li>
            <% }); %>
            </script>
            
            <script type="text/javascript" charset="utf-8">
            laut.fm.station('HIER DEINE STATION').last_songs({container:'songs', template:'songs_template'}, true);
            </script>
        </div>

        <div class="col-md-12">
        <div id="playlists"></div> <script type="text/html" id="playlists_template" charset="utf-8"> <p>seit <%= this.current_playlist.started_at.humanTimeShort() %> Uhr<br /> <strong><%= this.current_playlist.name %></strong><br /> <%= this.current_playlist.description %></p> <p>ab <%= this.next_playlist.started_at.humanTimeShort() %> Uhr<br /> <strong><%= this.next_playlist.name %></strong><br /> <%= this.next_playlist.description %></p> </script> <script type="text/javascript" charset="utf-8"> laut.fm.station('HIER DEINE STATION') .info({container:'playlists', template:'playlists_template'}, true); </script>
        </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" ></script>
</body>
</html>